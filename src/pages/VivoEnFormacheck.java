package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class VivoEnFormacheck extends TestBaseTG {
	
	final WebDriver driver;
	public VivoEnFormacheck(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("general__spinner"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInVivoEnForma(String apuntaA) {
		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Vivo En Forma - landing");
		
	WebElement imc = driver.findElement(By.xpath("//a[contains(text(), 'IMC')]"));
		imc.click();

		espera(900);
	
	WebElement inicio = driver.findElement(By.xpath("//a[contains(text(), 'Inicio')]"));
		inicio.click();
	
		espera(900);
		
	//WebElement FitnessSecion = driver.findElement(By.xpath("//a[contains(text(), 'Fitness')]"));
	//	FitnessSecion.click();
	
		espera(500);
	driver.get(apuntaA + "vivoenforma.com/fitness/ejercicios-basicos/");	
		String URL0 = driver.getCurrentUrl();
		Assert.assertEquals(URL0, "http://vivoenforma.com/fitness/ejercicios-basicos/" );
		espera(500); 	
	
	
	driver.get(apuntaA + "vivoenforma.com/fitness/rutinas-en-pareja/");
		String URL1 = driver.getCurrentUrl();
		Assert.assertEquals(URL1, "http://vivoenforma.com/fitness/rutinas-en-pareja/" );
		espera(500);
		
	
	driver.get(apuntaA + "vivoenforma.com/fitness/mama-fitness/");
		String URL2 = driver.getCurrentUrl();
		Assert.assertEquals(URL2, "http://vivoenforma.com/fitness/mama-fitness/" );
		espera(500);
		
	
	driver.get(apuntaA + "vivoenforma.com/nutricion-y-alimentacion/planes-nutricionales/");
		String URL3 = driver.getCurrentUrl();
		Assert.assertEquals(URL3, "http://vivoenforma.com/nutricion-y-alimentacion/planes-nutricionales/" );
		espera(500);
	
		
	driver.get(apuntaA + "vivoenforma.com/nutricion-y-alimentacion/recetas-saludables/");
		String URL4 = driver.getCurrentUrl();
		Assert.assertEquals(URL4, "http://vivoenforma.com/nutricion-y-alimentacion/recetas-saludables/" );
		espera(500);
		
		
	System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Vivo En Forma"); 
	System.out.println();
	System.out.println("Fin de Test Vivo En Forma - Landing");
		

	}		

}  

